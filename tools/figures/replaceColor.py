# -*- coding: utf-8 -*-

import sys, string

from colors import colorsDic


def replaceColor(inputFile, outputFile):
  fi = file(inputFile, "r")
  imgData = fi.read()
  fi.close()
  
  for it in colorsDic:
      imgData = imgData.replace(it['old'], it['new'])

  fo = file(outputFile, "w")
  fo.write(imgData)
  fo.close()


if len(sys.argv) != 3:
  usage()
  sys.exit()


def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file>'
  print '    input file:   Input image in svg format'
  print '    output file:  Output image in svg format'
  
  
#try:
replaceColor(sys.argv[1], sys.argv[2])
#except:
#    usage()
