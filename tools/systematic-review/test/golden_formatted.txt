---- ---- ---- ----
MARK (0 out 100 in) : 
GROUPS              : 
RULES               : 
NAME                : 
PROBLEMS            : 
COMMENTS            : 

TITLE    : Hybrid functional verification methodology for video/audio SoC

AUTHORS  : Gupta, S.
KEY      : 5206256
TYPE     : inproceedings
YEAR     : 2009
PAGES    : 264-265
CONGRESS : Quality Electronic Design, 2009. ASQED 2009. 1st Asia Symposium on

ABSTRACT : Functional verification (namely early verification of multimedia processing capabilities) is one of the main challenges in developing SoC-based products, such as consumer electronic devices and portables that incorporate complex audio and video interfaces. Due to rising design complexity, increasingly intricate hardware/software interactions and rising demand for lower power operation are putting pressure on SoC functional verification strategies. These trends are the threats to SoC predictability and product development schedules. In this paper I am discussing hybrid functional verification methodology well suited for Video/Audio SoC.
#### #### #### ####


---- ---- ---- ----
MARK (0 out 100 in) : 
GROUPS              : 
RULES               : 
NAME                : 
PROBLEMS            : 
COMMENTS            : 

TITLE    : Directed-simulation assisted formal verification of serial protocol and bridge

AUTHORS  : Gorai, S. and Biswas, S. and Bhatia, L. and Tiwari, P. and Mitra, R.S.
KEY      : 1688893
TYPE     : inproceedings
YEAR     : 2006
PAGES    : 731-736
CONGRESS : Design Automation Conference, 2006 43rd ACM/IEEE

ABSTRACT : Robust verification of protocol conversion and arbitration schemes of SoC bridges forms a significant component of the overall SoC verification. Formal verification provides a way to achieve this, but a naive approach often leads to explosion of the state space, and is impractical for most of today's protocols and bridges. This problem is further complicated in the presence of serial protocols, where control and data are mixed together and transactions continue for very great depths. White-box verification is not a feasible solution, since these bridges are often imported or generated from other sources, and internal information is not readily available. In this paper, we propose a black-box and hybrid approach to this problem, by judiciously mixing simulation and formal verification. We illustrate our approach by applying it to two dual stage bridges that perform serial to parallel protocol conversion and vice versa
#### #### #### ####


---- ---- ---- ----
MARK (0 out 100 in) : 
GROUPS              : 
RULES               : 
NAME                : 
PROBLEMS            : 
COMMENTS            : 

TITLE    : Fast and scalable hybrid functional verification and debug with dynamically reconfigurable co-simulation

AUTHORS  : Banerjee, S. and Gupta, T.
KEY      : 6386597
TYPE     : inproceedings
YEAR     : 2012
PAGES    : 115-122
CONGRESS : Computer-Aided Design (ICCAD), 2012 IEEE/ACM International Conference on

ABSTRACT : Hybrid functional verification and debug systems which combine high execution speed of logic emulators and full observability and controllability of software simulators are widely used, but suffer from scalability problem since software simulators cannot handle large and complex System-on-chip (SoC) designs efficiently, restricting their application to only relatively small designs. This paper presents a completely scalable hybrid verification and debug system based on dynamically reconfigurable co-simulation. Unlike existing systems, it allows one or more component logic blocks of a SoC to run on simulator for debugging while rest of the design still runs on emulator. The full design under test (DUT) is run on emulator at near hardware speed for long test sequences, and on error detection one or more logic blocks are transparently switched over to simulation for debugging, initializing the system as a piecewise co-simulator. Logic blocks can be flexibly relocated between simulator and emulator dynamically, without going through time consuming design recompilation phase, allowing designers to quickly debug functional issues. Application of the system to verification of real complex designs shows the effectiveness of our approach.
#### #### #### ####


---- ---- ---- ----
MARK (0 out 100 in) : 
GROUPS              : 
RULES               : 
NAME                : 
PROBLEMS            : 
COMMENTS            : 

TITLE    : FEMU: A firmware-based emulation framework for SoC verification

AUTHORS  : Li, Hao and Tong, Dong and Huang, Kan and Cheng, Xu
KEY      : 5751510
TYPE     : inproceedings
YEAR     : 2010
PAGES    : 257-266
CONGRESS : Hardware/Software Codesign and System Synthesis (CODES+ISSS), 2010 IEEE/ACM/IFIP International Conference on

ABSTRACT : Full-system emulation on FPGA(Field-Programmable Gate Array) with real-world workloads can enhance the confidence of SoC(System-on-Chip) design. However, since FPGA emulation requires complete implementation of key modules and provides weak visibility, it is time-consuming. This paper proposes FEMU, a hybrid firmware/hardware emulation framework for SoC verification. The core of FEMU is implemented by transplanting QEMU, a full-system emulator, from OS level to BIOS level, so we can directly emulate devices upon hardware. Moreover, FEMU provides programming interfaces to simplify device modeling in firmware. Based on an auxiliary set of hardware modules, FEMU allows hybrid full-system emulation with the combination of real hardware and emulated firmware model. Therefore, FEMU can facilitate full-system emulation in three aspects. First, FEMU enables full-system emulation with the minimum hardware implementation, so the DUT (Design Under Test) module can be verified under target application as early as possible. Second, by comparing the execution traces generated using real hardware and emulated firmware model, respectively, FEMU helps locate and fix bugs occurred in the full-system emulation. Third, by replacing un-verified hardware modules with emulated firmware models, FEMU helps isolating design issues in multiple modules. In a practical SoC project, FEMU helped us identify several design issues in full-system emulation. In addition, the evaluation results show that the emulation speed of FEMU is comparable with QEMU after transplantation.
#### #### #### ####


---- ---- ---- ----
MARK (0 out 100 in) : 
GROUPS              : 
RULES               : 
NAME                : 
PROBLEMS            : 
COMMENTS            : 

TITLE    : A scalable hybrid verification system based on HDL slicing

AUTHORS  : Banerjee, S. and Gupta, T. and Jain, S.
KEY      : 6114164
TYPE     : inproceedings
YEAR     : 2011
PAGES    : 41-48
CONGRESS : High Level Design Validation and Test Workshop (HLDVT), 2011 IEEE International

ABSTRACT : For functional verification, logic emulators offer speed of execution while software simulators provide full observability and advanced debugging techniques. Hybrid functional verification systems, which run long test sequences in an emulator and upon error detection, transparently switch-over to simulation based debugging are extensively used. These systems suffer from scalability problem since simulators cannot handle large designs efficiently, restricting their application to only relatively small designs. This paper presents a novel methodology to achieve scalability in such systems. The full design is run on emulator, but on error detection only a smaller and pre-computed HDL slice corresponding to the property under verification is loaded on simulator for debugging, instead of the full unsliced design. Application of the system to verification of real complex System-on-Chips (SoC) show the effectiveness of our approach.
#### #### #### ####


---- ---- ---- ----
MARK (0 out 100 in) : 
GROUPS              : 
RULES               : 
NAME                : 
PROBLEMS            : 
COMMENTS            : 

TITLE    : System prototypes: Virtual, hardware or hybrid?

AUTHORS  : Borgstrom, T. and Haritan, E. and Wilson, R. and Abada, D. and Chandra, R. and Cruse, C. and Dauman, A. and Mielo, O. and Nohl, A.
KEY      : 5227212
TYPE     : inproceedings
YEAR     : 2009
PAGES    : 1-3
CONGRESS : Design Automation Conference, 2009. DAC '09. 46th ACM/IEEE

ABSTRACT : Almost all SoC designs today use hardware prototyping at some point of the development cycle to perform hardware/software validation or test interfaces to real-world stimulus. Recently, virtual prototypes have emerged as a way to run system level tests and perform hardware/software co-simulation before silicon or hardware prototypes are available. Does the emergence of virtual prototyping mean the end for FPGA-based prototypes? Will the hardware prototype continue to live on as an integral part of the SoC design cycle? Will hybrids of virtual and hardware prototypes be the answer? Join our expert panelists in a spirited discussion on the best approach for speeding the verification and validation of complex SoC using system prototypes. Virtual, hardware or hybrid - which will reign supreme?
#### #### #### ####


