# -*- coding: utf-8 -*-

import sys

MARK_OFFSET = 1
KEY_OFFSET = 11
YEAR_OFFSET = 13

ENTRY_INFO_OFFSET = 11
AUTHOR_INFO_OFFSET = 22


def check(x,y):
  if x[1]>y[1]:
    return -1
  return 1



def sortEntries(inputFile, outputFile, listKEY, listYEAR, listMARK, bestArticles):
  fi = file(inputFile, "r")
  text = fi.readlines()
  fi.close()

  totalEntries = text.count("---- ---- ---- ----\n")
  entries=[[]]*totalEntries
  indexEntries = [0]*totalEntries
  lastIndex=0


  for i in range(0,totalEntries):
    index = text.index("---- ---- ---- ----\n",lastIndex)
    indexEntries[i] = index
    try:
      entries[i]=[i,int(text[index+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1])]
    except:
      entries[i]=[i,0]

    lastIndex=index+1

  entries.sort(check)

  fo = file(outputFile, "w")
  cnt = 0


  for item in entries:
    initIndex = indexEntries[item[0]]
    index = text.index("#### #### #### ####\n",initIndex)
    lines = text[initIndex:index+1]
    if (cnt < bestArticles):
      listKEY.append(int(text[initIndex+KEY_OFFSET][ENTRY_INFO_OFFSET:-1]))
      listYEAR.append(int(text[initIndex+YEAR_OFFSET][ENTRY_INFO_OFFSET:-1]))
      listMARK.append(int(text[initIndex+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1]))
    for line in lines:
      fo.write(line)
    if (cnt==bestArticles):
      fo.write("**** Above of this line are the best articles ****\n")
    cnt +=1

  fo.close()



def topChart(listKEY, listYEAR, listMARK):
  foChart = file("bestArticles.txt", "w")
  foChart.write("\n\n")

#  for item in listKEY:
  for i in range(0,listKEY.__len__()):
    foChart.write(str(i) + "  " + "http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber="+str(listKEY[i])
                  + "  " + str(listYEAR[i]) + "  " + str(listMARK[i]) + "\n")

  foChart.write("\n\n\nArticles\n\n")

  for item in listKEY:
    foChart.write("http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber="+str(item)+"\n")

  foChart.write("\n\n\nYears\n\n")

  for i in range(0,listKEY.__len__()):
    foChart.write(str(listKEY[i]) + " " + str(listYEAR[i])+"\n")

  foChart.write("\n\n\nCMD\n\n")
  for i in range(0,listKEY.__len__()):
    foChart.write("mv 0"+str(listKEY[i]) + ".pdf " + str(i)+"_0"+str(listKEY[i])+".pdf\n")

  foChart.close()



def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file> <bestArticles>'
  print '    input file:   File obtained from format application'
  print '    output file:  Output file which contains the articles in order'
  print '    bestArticles: Number of articles that will be included into top chart'






if len(sys.argv) != 4:
  usage()
  sys.exit()


listKEY = []
listYEAR = []
listMARK = []


try:
  sortEntries(sys.argv[1], sys.argv[2], listKEY, listYEAR, listMARK, int(sys.argv[3]))
  topChart(listKEY, listYEAR, listMARK)
except:
  usage()

