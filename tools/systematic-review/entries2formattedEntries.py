# -*- coding: utf-8 -*-

from pybtex.database.input import bibtex
import sys



def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file>'
  print '   input file:  Input bibtex file'
  print '   output file: Output file, which contains a special format to evaluate the articles of input file'  



def setFormat(inputFile, outputFile):
  parser = bibtex.Parser()
  bib_data = parser.parse_file(inputFile)

  cnt = 0 
  txt = ""

  for i in bib_data.entries:
    try:
      entry = bib_data.entries.get(i)
      fields = entry.fields
      article = ""

      article += "---- ---- ---- ----"  + "\n"
      article += "MARK (0 out 100 in) : " + "\n"
      article += "GROUPS              : " + "\n"
      article += "RULES               : " + "\n"
      article += "INFO                : " + "\n"
      article += "PROBLEMS            : " + "\n"
      article += "RELEVANT            : " + "\n\n"

      article += "TITLE    : " + str(fields["title"]) + "\n\n"

      article += "AUTHORS  : " + str(fields["author"])  + "\n"
      article += "KEY      : " + str(entry.key)  + "\n"
      article += "TYPE     : " + str(entry.type) + "\n"
      article += "YEAR     : " + str(fields["year"]) + "\n"
      article += "PAGES    : " + str(fields["pages"]) + "\n"
      if "journal" in fields:
        article += "JOURNAL  : " + str(entry.fields["journal"]) + "\n"
      if "booktitle" in fields:
        article += "CONGRESS : " + str(entry.fields["booktitle"]) + "\n"

      article += "\n"
      article += "ABSTRACT : " + str(entry.fields["abstract"]) + "\n"
      article += "#### #### #### ####"  + "\n\n\n"
      txt += article
      cnt +=1
    except:
      print "Error in article, which its ID is " + str(entry.key) 

    f = open(sys.argv[2], 'w')
    f.write(txt)
    f.close()

  print 'Total of studies: ' + str(cnt)




if len(sys.argv) != 3:
  usage()
  sys.exit()

setFormat(sys.argv[1], sys.argv[2])
