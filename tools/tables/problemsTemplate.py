# -*- coding: utf-8 -*-

areas = {}
acronym = {}

areas["Software"]=[]
areas["Técnicas"]=[]
areas["Diseño hardware"]=[]
areas["HW"]=[]


areas["Software"].append("Soporte Ágil")
acronym["Soporte Ágil"]= "agil"
areas["Software"].append("Reducción de breakpoints")
acronym["Reducción de breakpoints"]= "breakpoints"
areas["Software"].append("Orientación a objetos")
acronym["Orientación a objetos"]= "oop"


areas["Técnicas"].append("Proceso metodológico")
acronym["Proceso metodológico"]= "uvm"
areas["Técnicas"].append("Cobertura hardware")
acronym["Cobertura hardware"]= "coverage"
areas["Técnicas"].append("Estimulación errónea")
acronym["Estimulación errónea"]= "faultInjection"


areas["Diseño hardware"].append("Bajo nivel de abstracción")
acronym["Bajo nivel de abstracción"]= "lowLevel"
areas["Diseño hardware"].append("Mejora del time-to-market")
acronym["Mejora del time-to-market"]= "t2m"
areas["Diseño hardware"].append("Incremento de la complejidad")
acronym["Incremento de la complejidad"]= "socDesign"
areas["Diseño hardware"].append("Cuello de botella: Verificación")
acronym["Cuello de botella: Verificación"]= "verifProcess"
areas["Diseño hardware"].append("Minimizar síntesis")
acronym["Minimizar síntesis"]= "minSynth"
areas["Diseño hardware"].append("Baja visibilidad")
acronym["Baja visibilidad"]= "debug"
areas["Diseño hardware"].append("Temporalización")
acronym["Temporalización"]= "time"


areas["HW"].append("Uso de checkers")
acronym["Uso de checkers"]= "checkers"
areas["HW"].append("Uso de hardware real")
acronym["Uso de hardware real"]= "realHW"





