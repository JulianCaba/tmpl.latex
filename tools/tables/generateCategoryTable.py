# -*- coding: utf-8 -*-
import sys
from areas import areas, acronym


MARK_OFFSET = 1
GROUPS_OFFSET = 2
RULES_OFFSET = 3
NAME_OFFSET = 4
PROBLEMS_OFFSET = 5

TITLE_OFFSET = 8
AUTHORS_OFFSET = 10
KEY_OFFSET = 11
TYPE_OFFSET = 12
YEAR_OFFSET = 13
PUBLISHER_OFFSET = 15

ENTRY_INFO_OFFSET = 11
AUTHOR_INFO_OFFSET = 22



def getEntries(inputFile, thershold, entries):
  fi = file(inputFile, "r")
  text = fi.readlines()
  fi.close()

  totalPapers = text.count("---- ---- ---- ----\n")
  lastIndex=0

  for i in range(0,totalPapers):
    index = text.index("---- ---- ---- ----\n",lastIndex)
    entry = ""
    try:
      mark = int(text[index+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1])
    except:
      mark = 0

    if mark >= thershold:
      entry += text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + ' '
      entry += text[index+NAME_OFFSET][AUTHOR_INFO_OFFSET:-1] + ' '
      entry += text[index+GROUPS_OFFSET][AUTHOR_INFO_OFFSET:-1] + '\n'
      entries.append(entry)
    lastIndex = index + 1


def getColumns():
#Columnas
  columns = 0
  for i in areas.keys():
    columns += areas[i].__len__()  

  return columns


def printHeaderTable(outputFile, columns):
#Primera linea
  header = ""
  color = True
  columnsFormat = "" 
  
  for i in areas.keys():
    for j in areas[i]:
      if color:
        columnsFormat += ">{\\columncolor{gray90}}"
      columnsFormat += "c "
    color = not color
  
  header += "\\begin{center}\n"
  header += "\\begin{landscape}\n"
  header += "\\begin{longtable}{p{2cm} " + columnsFormat + "}\n"
  header += "\\toprule[0.5mm]\n\n"
  header += "\\multirow{3}{*}{\\textbf{Nombre}}  &\n"
  header += "\\multicolumn{"+ str(columns)+"}{c}{\\textbf{Categorías}}\n"
  header += "\\\\\n\n"

  outputFile.write(header)



def printAreas(outputFile, columns):
#Inclusion Areas
  color = True
  headerAreas = ""

  for i in areas.keys():
    headerAreas += "& \\multicolumn{"+str(areas[i].__len__())+"}{"
    if color:
      headerAreas += ">{\columncolor{gray75}}"
    headerAreas += "c}{\\textbf{\\textit{"+i+"}}}\n"
    color = not color

  headerAreas += "\\\\\n\n"
 # headerAreas += "\\cline{2-"+ str(columns+1)+"}\n\n"

  outputFile.write(headerAreas)



def printFeatures(outputFile, featuresList, columns):
#Inclusion Caracteristicas
#  columnSplits = "&"*columns
  features = ""

#  features += columnSplits + "\\\\\n\n"

  for i in areas.keys():
    for j in areas[i]:
      features +=  "& \\begin{sideways} \\textit{" + j + "} \\end{sideways}\n"
      featuresList.append(j)

  features += "\\\\\n\n"
  features += "\\midrule[0.5mm]\n\n"

  outputFile.write(features)



def printEntry(outputFile, featuresList):
#Inclusion de cada propuesta
  rows=""
  color = False

  for line in entries:
    try:   
      splitLine = line.split()
      reference = splitLine[0]
      name = splitLine[1]
      properties = splitLine[2:]

      if color:
        rows += "\\rowcolor{gray75}\n"
      rows += "\\textbf{" + name + "}\\cite{" + reference + "} "
      for i in featuresList:
        if acronym[i] in properties:
          rows += " & \\checkmark "
        else:
          rows += " & - "
      rows += "\\\\\n\n"
      color = not color
    except:
      print "Error in " + line + "\n"

  outputFile.write(rows)



def printTail(outputFile):
#Final
  tail = ""

  tail += "\\end{longtable}\n"
  tail += "\\end{landscape}\n"
  tail += "\\end{center}\n\n"

  tail += "\\clearpage\n\n"

  outputFile.write(tail)




def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file> <thershold>'
  print '    input file:   File obtained from format application with author information'
  print '    output file:  Output file which contains a table with categorized studies'
  print '    thershold:    Minimum thershold of mark assigned (between 1 and 100)'




if len(sys.argv) != 4:
  usage()
  sys.exit()


entries = []
featuresList = []

try:
  if int(sys.argv[3]) > 0 and int(sys.argv[3]) <= 100:
    getEntries(sys.argv[1], int(sys.argv[3]), entries)    
  else:
    print 'Wrong Thershold (Thershold should be between 1 and 100)'

  of = file(sys.argv[2], "w")
  
  printHeaderTable(of, getColumns())
  printAreas(of, getColumns())
  printFeatures(of, featuresList, getColumns())
  printEntry(of, featuresList)
  printTail(of)

  of.close()
  print 'It has generated a LaTeX file called ' + sys.argv[2] + ' This file contains a table with all relevant studies, which have been categorized'
except:
  usage()
