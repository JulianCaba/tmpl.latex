# -*- coding: utf-8 -*-

import sys

MARK_OFFSET = 1
GROUPS_OFFSET = 2
RULES_OFFSET = 3
INFO_OFFSET = 4
PROBLEMS_OFFSET = 5
RELEVANT_OFFSET = 6

TITLE_OFFSET = 8
AUTHORS_OFFSET = 10
KEY_OFFSET = 11
TYPE_OFFSET = 12
YEAR_OFFSET = 13
PUBLISHER_OFFSET = 15

ENTRY_INFO_OFFSET = 11
AUTHOR_INFO_OFFSET = 22



def genTables(inputFile, outputFile, thershold, glFlag):
  listKEY = []
  listYEAR = []

  fi = file(inputFile, "r")
  text = fi.readlines()
  fi.close()

  totalPapers = text.count("---- ---- ---- ----\n")
  lastIndex=0

  fo = file(outputFile, "w")

  entry = "\\begin{itemize}\n"

  for i in range(0,totalPapers):
    index = text.index("---- ---- ---- ----\n",lastIndex)
    try:
      mark = int(text[index+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1])
    except:
      mark = 0

    if mark >= thershold:
      entry += '\\item ['
      if (glFlag or text[index+RELEVANT_OFFSET][AUTHOR_INFO_OFFSET:-1] == 'Si'):
        entry += '\\green{\\ding{52}}'#Checkmark'
      else:
        entry += '\\red{\\ding{56}}'#XSolidBrush'
        
      entry += ']\\cite{' + text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + '} \\textit{\\textquotedblleft '
      entry += text[index+TITLE_OFFSET][ENTRY_INFO_OFFSET:-1]
      entry += '\\textquotedblright }\n'
    
    lastIndex=index+1

  entry += "\\end{itemize}\n\n"
  fo.write(entry);
  
  fo.close()



def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file> <thershold> <grayLiterature>'
  print '    input file:   File obtained from format application with author information'
  print '    output file:  Output file which contains a table with each paper'
  print '    thershold:    Minimum thershold of mark assigned (between 1 and 100)'




if len(sys.argv) < 4:
  usage()
  sys.exit()


try:
  if int(sys.argv[3]) > 0 and int(sys.argv[3]) <= 100:
    grayLiterature = False
    if len(sys.argv) != 4:
      grayLiterature = True
    genTables(sys.argv[1], sys.argv[2], int(sys.argv[3]), grayLiterature)
   
      
    print 'It has generated a LaTeX file called ' + sys.argv[2] + ' This file contains information of primary studies'
  else:
    print 'Wrong Thershold (Thershold should be between 1 and 100)'
except:
  usage()
