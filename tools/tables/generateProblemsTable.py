# -*- coding: utf-8 -*-
import sys
from problems import areas, acronym


MARK_OFFSET = 1
GROUPS_OFFSET = 2
RULES_OFFSET = 3
NAME_OFFSET = 4
PROBLEMS_OFFSET = 5

TITLE_OFFSET = 8
AUTHORS_OFFSET = 10
KEY_OFFSET = 11
TYPE_OFFSET = 12
YEAR_OFFSET = 13
PUBLISHER_OFFSET = 15

ENTRY_INFO_OFFSET = 11
AUTHOR_INFO_OFFSET = 22



def getEntries(inputFile, thershold, entries):
  fi = file(inputFile, "r")
  text = fi.readlines()
  fi.close()

  totalPapers = text.count("---- ---- ---- ----\n")
  lastIndex=0

  for i in range(0,totalPapers):
    index = text.index("---- ---- ---- ----\n",lastIndex)
    entry = ""
    try:
      mark = int(text[index+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1])
    except:
      mark = 0

    if mark >= thershold:
      entry += text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + ' '
      entry += text[index+PROBLEMS_OFFSET][AUTHOR_INFO_OFFSET:-1] + '\n'
      entries.append(entry)
    lastIndex = index + 1




def getColumns():
#Columnas
  columns = 0
  for i in areas.keys():
    columns += areas[i].__len__()  

  return columns



def printHeaderTable(outputFile, columns):
#Primera linea
  header = ""
  color = True
  columnsFormat = "" 
  
  for i in areas.keys():
    for j in areas[i]:
      if color:
        columnsFormat += ">{\\columncolor{gray90}}"
      columnsFormat += "c "
    color = not color
    columnsFormat += "| "
  
#  header += "\\newpage\n\n"
  header += "\\begin{center}\n"
#  header += "\\begin{landscape}\n"
  header += "\\begin{longtable}{p{2.3cm} | " + columnsFormat + "}\n"
  #header += "\\toprule[0.5mm]\n\n"
  #header += "\\multirow{3}{*}{\\textbf{Nombre}}  &\n"
  #header += "\\multicolumn{"+ str(columns)+"}{c}{\\textbf{Categorías}}\n"
  header += "\n\n"

  outputFile.write(header)



def printAreas(outputFile, columns):
#Inclusion Areas
  color = True
  headerAreas = ""

  for i in areas.keys():
    headerAreas += "& \\multicolumn{"+str(areas[i].__len__())+"}{"
    headerAreas += ">{\columncolor{"
    if color:
      headerAreas += "gray75"
    else:
      headerAreas += "gray90"
    headerAreas += "}}c|}{\scriptsize\\textbf{\\textit{"+i+"}}}\n"
    color = not color

  headerAreas += "\\\\\n\n"
 # headerAreas += "\\cline{2-"+ str(columns+1)+"}\n\n"

  outputFile.write(headerAreas)



def printFeatures(outputFile, featuresList, columns):
#Inclusion Caracteristicas
#  columnSplits = "&"*columns
  features = ""

#  features += columnSplits + "\\\\\n\n"
  
  features += "\cmidrule{2-" + str(getColumns()+1) + "}\n\n"
  features += "\\textbf{Article}\n"
  for i in areas.keys():
    for j in areas[i]:
      features +=  "& \\begin{sideways} \\textit{" + j + "} \\end{sideways}\n"
      featuresList.append(j)

  features += "\\\\\n\n"
  features += "\\midrule[0.5mm]\n\n"

  outputFile.write(features)



def printEntry(outputFile, featuresList, subentries):
#Inclusion de cada propuesta
  rows=""
  color = False

  print acronym
  for line in subentries:
    try:   
      splitLine = line.split()
      reference = splitLine[0]
      problems = splitLine[1:]

      if color:
        rows += "\\rowcolor{gray75}\n"
      rows += "\\cite{" + reference + "} "
      for i in acronym:#featuresList:
        if acronym[i] in problems:
          rows += " & \\checkmark "
        else:
          rows += " & - "
      rows += "\\\\\n\n"
      color = not color
    except:
      print "Error in " + line + "\n"

  outputFile.write(rows)



def printTail(outputFile, idTable, nameTable, label):
#Final
  tail = ""

  tail += "\\midrule[0.5mm]\n\n"
  tail += "\\caption{" + nameTable
  if idTable != 0:
    tail += "(" + str(idTable) +")"
  tail += "}\n\\label{tab:" + label + str(idTable) + "}\n"
  tail += "\\end{longtable}\n"
#  tail += "\\end{landscape}\n"
  tail += "\\end{center}\n\n"


  outputFile.write(tail)




def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file> <thershold> <caption> <label>'
  print '    input file:   File obtained from format application with author information'
  print '    output file:  Output file which contains a table with categorized studies'
  print '    thershold:    Minimum thershold of mark assigned (between 1 and 100)'




if len(sys.argv) != 6:
  usage()
  sys.exit()


entries = []
featuresList = []

try:
  if int(sys.argv[3]) > 0 and int(sys.argv[3]) <= 100:
    getEntries(sys.argv[1], int(sys.argv[3]), entries)    
  else:
    print 'Wrong Thershold (Thershold should be between 1 and 100)'

  of = file(sys.argv[2], "w")
  
  i = 0
  j = 1
  while(1):
    featuresList = []
    printHeaderTable(of, getColumns())
    printAreas(of, getColumns())
    printFeatures(of, featuresList, getColumns())
    printEntry(of, featuresList, entries[i:i+27])
    if len(entries) <= 27:
      j = 0
    printTail(of, j, sys.argv[4], sys.argv[5])
    if (i+27 >= len(entries)):
      break
    i = i + 27
    j = j + 1

  of.close()
  print 'It has generated a LaTeX file called ' + sys.argv[2] + '.\nThis file contains a table with all relevant studies and their problems.'
except:
  usage()
