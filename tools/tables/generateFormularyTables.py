# -*- coding: utf-8 -*-

import sys

MARK_OFFSET = 1
GROUPS_OFFSET = 2
RULES_OFFSET = 3
NAME_OFFSET = 4

TITLE_OFFSET = 8
AUTHORS_OFFSET = 10
KEY_OFFSET = 11
TYPE_OFFSET = 12
YEAR_OFFSET = 13
PUBLISHER_OFFSET = 15

ENTRY_INFO_OFFSET = 11
AUTHOR_INFO_OFFSET = 22



def genTables(inputFile, outputFile, thershold):
  listKEY = []
  listYEAR = []

  fi = file(inputFile, "r")
  text = fi.readlines()
  fi.close()

  totalPapers = text.count("---- ---- ---- ----\n")
  lastIndex=0

  fo = file(outputFile, "w")

  for i in range(0,totalPapers):
    index = text.index("---- ---- ---- ----\n",lastIndex)
    entry = ""
    try:
      mark = int(text[index+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1])
    except:
      mark = 0

    if mark >= thershold:
      entry += '%% Entry: ' + text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + '\n'
      entry += '\\gbox{\\textit{'
      entry += text[index+TITLE_OFFSET][ENTRY_INFO_OFFSET:-1]
      entry += '}}\n\n'

      entry += '\\begin{table}[ht!]\n' 
      entry += '\\begin{center}\n'
      entry += '\\begin{tabulary}{16cm}{R p{12cm}}\n\n'

      entry += '\\toprule[0.5mm]\n\n'

      entry += '\\rowcolor{gray90}\n'
      entry += '\\textbf{Autores} & \n'
      entry += text[index+AUTHORS_OFFSET][ENTRY_INFO_OFFSET:-1] + ' \\\\\n\n'

      entry += '\\textbf{Entidad} & \n  \\\\\n\n'

      entry += '\\rowcolor{gray90}\n'
      entry += '\\textbf{Fecha} & \n'
      entry += text[index+YEAR_OFFSET][ENTRY_INFO_OFFSET:-1] + ' \\\\\n\n'

      entry += '\\textbf{Fuente} & \n'
      entry += text[index+PUBLISHER_OFFSET][ENTRY_INFO_OFFSET:-1] + ' \\\\\n\n'

      entry += '\\rowcolor{gray90}\n'
      entry += '\\textbf{Categoría} & \n'
      entry += text[index+GROUPS_OFFSET][AUTHOR_INFO_OFFSET:-1] + ' \\\\\n\n'

      entry += '\\textbf{Criterio} & \n'
      entry += text[index+RULES_OFFSET][AUTHOR_INFO_OFFSET:-1] + ' \\\\\n\n'

      entry += '\\rowcolor{gray90}\n'
      entry += '\\textbf{Problema} & \n'
      entry += '\\begin{itemizePacket}\n'
      entry += '\item\n'
      entry += '\end{itemizePacket}\n \\\\\n\n'

      entry += '\\textbf{Propuesta} & \n \\\\\n\n'

      entry += '\\rowcolor{gray90}\n'
      entry += '\\textbf{Relevante} & \n \\\\\n\n'
      entry += '\\textbf{Referencia} & \n'
      entry += '\\cite{' + text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + '}\\\\\n\n'

      entry += '\\bottomrule[0.5mm]\n'
      entry += '\\end{tabulary}\n'
      entry += '\\end{center}\n'
      entry += '\\label{tab:entry' + text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + '}\n'
      entry += '\\end{table}\n\n\n'
      entry += '%%%% %%%% %%%% %%%% %%%% %%%%\n\n\n'
      fo.write(entry);
    
    lastIndex=index+1

  fo.close()



def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file> <thershold>'
  print '    input file:   File obtained from format application with author information'
  print '    output file:  Output file which contains a table with each paper'
  print '    thershold:    Minimum thershold of mark assigned (between 1 and 100)'




if len(sys.argv) != 4:
  usage()
  sys.exit()


try:
  if int(sys.argv[3]) > 0 and int(sys.argv[3]) <= 100:
    genTables(sys.argv[1], sys.argv[2], int(sys.argv[3]))
    print 'It has generated a LaTeX file called ' + sys.argv[2] + ' This file contains information of primary studies'
  else:
    print 'Wrong Thershold (Thershold should be between 1 and 100)'
except:
  usage()
