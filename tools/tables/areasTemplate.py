# -*- coding: utf-8 -*-

areas = {}
acronym = {}

areas["Area 1"]=[]
areas["Area 2"]=[]

areas["Area 1"].append("Group A")
acronym["Group A"]= "P1"

areas["Area 1"].append("Group B")
acronym["Group B"]= "P2"

areas["Area 1"].append("Group C")
acronym["Group C"]= "P3"

areas["Area 2"].append("Group X")
acronym["Group X"]= "P4"

areas["Area 2"].append("Group Y")
acronym["Group Y"]= "P5"
