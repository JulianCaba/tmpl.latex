# -*- coding: utf-8 -*-

import sys

MARK_OFFSET = 1
GROUPS_OFFSET = 2
RULES_OFFSET = 3
NAME_OFFSET = 4

TITLE_OFFSET = 8
AUTHORS_OFFSET = 10
KEY_OFFSET = 11
TYPE_OFFSET = 12
YEAR_OFFSET = 13
PUBLISHER_OFFSET = 15

ENTRY_INFO_OFFSET = 11
AUTHOR_INFO_OFFSET = 22



def genBibtex(inputFile, outputFile):
  fi = file(inputFile, "r")
  text = fi.readlines()
  fi.close()

  totalPapers = text.count("---- ---- ---- ----\n")
  lastIndex=0

  fo = file(outputFile, "w")

  entry = '\n\n'
  entry += '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' 
  entry += '%%  Bibtex System Review\n'
  entry += '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'
  fo.write(entry);

  for i in range(0,totalPapers):
    index = text.index("---- ---- ---- ----\n",lastIndex)
    entry = ""
    try:
      mark = int(text[index+MARK_OFFSET][AUTHOR_INFO_OFFSET:-1])
    except:
      mark = 0

    if mark > 0:
      entry += '%% Entry: ' + text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + '\n'
      entry += '@Article{' + text[index+KEY_OFFSET][ENTRY_INFO_OFFSET:-1] + ',\n'
      entry += '  author  = {' + text[index+AUTHORS_OFFSET][ENTRY_INFO_OFFSET:-1] +'},\n'
      entry += '  title   = {' + text[index+TITLE_OFFSET][ENTRY_INFO_OFFSET:-1] + '},\n'
      entry += '  journal = {' + text[index+PUBLISHER_OFFSET][ENTRY_INFO_OFFSET:-1] + '},\n'
      entry += '  year    = {' + text[index+YEAR_OFFSET][ENTRY_INFO_OFFSET:-1] + '}\n'
      entry += '}\n\n'
      fo.write(entry);
    
    lastIndex=index+1

  entry = '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n'
  fo.write(entry);

  fo.close()



def usage():
  print 'Usage: ' + sys.argv[0] + ' <input file> <output file>'
  print '    input file:   File obtained from format application with author information'
  print '    output file:  Output file which contains bibtex file'



if len(sys.argv) != 3:
  usage()
  sys.exit()


try:
  genBibtex(sys.argv[1], sys.argv[2])
  print 'It has generated a LaTeX file called ' + sys.argv[2] + ' This file contains the bibtex'
except:
  usage()
