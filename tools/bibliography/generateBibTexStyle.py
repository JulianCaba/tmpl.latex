# -*- coding: utf-8 -*-

import sys



def genBibStyle(bibFile):
    fi = file(bibFile, "r")

    style = ""
    block = True
    bibBlock = True
    strBlock = ""

    for line in fi:
        if len(line)==1:
            continue
        elif line.count("newcommand") == 1:
            continue
        elif line.count("\etalchar") == 1:
            if line.count("thebibliography") == 1:
                style += line.replace("{\etalchar{+}}", "X")
            else:
                style += line.replace("{\etalchar{+}}", "")
        elif line.count("end")==1:
            style += "\\\\\n" + strBlock + "\\\\\n\n" + line
        elif line.count("thebibliography")==1:
            style += line
        elif line.count("bibitem")==1:
            if block and not bibBlock:
                style += "\\\\\n" + strBlock + "\\\\\n" 
            style += "\n\n" + line
            strBlock = ""
            block = False
            bibBlock = False
        elif line.count("newblock")==1:            
            if block:
                style += "\\\\\n"+strBlock
            block = True
            strBlock = line[:-1]
        elif not block:
                #authors = line.replace(" and ", " y ")
                style += "{\\bf " + authors[:-1] + "}"
        else:
            strBlock += line[:-1]
               
    fi.close()

    fo = file(bibFile, "w")     
    fo.write(style)
    fo.close()




def usage():
  print 'Usage: ' + sys.argv[0] + ' <bibtex file>'
  print '    bibtex file:  File obtained after bibliography file parsed with bibtex tool'





if len(sys.argv) != 2:
  usage()
  sys.exit()


#try:
genBibStyle(sys.argv[1])
print 'It has generated a new bibTex style called ' + sys.argv[1]
#except:
#  usage()


