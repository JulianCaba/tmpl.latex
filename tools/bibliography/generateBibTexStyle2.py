# -*- coding: utf-8 -*-

import sys



def genBibStyle(bibFile):
    fi = file(bibFile, "r")

    style = ""
    newBlock = False
    newItem = False
    firstBlock = True

    for line in fi:

        if line.count("newblock") == 1:
            newBlock = True
            if firstBlock:
                firstBlock = False
                style = style[:-1] + "} \\\\\n"
        elif line.count("bibitem") == 1:
            newBlock = False
            newItem = True

        if line.count("newcommand") == 1:
            continue
        elif line.count("\etalchar") == 1:
            if line.count("thebibliography") == 1:
                style += line.replace("{\etalchar{+}}", "X")
            else:
                style += line.replace("{\etalchar{+}}", "") + "{\\bf "
        elif len(line) == 1 and line.count("\n") == 1:
            newItem = False
            newBlock = False
            firstBlock = True
            style += "\n"    
        elif line.count("newblock") == 0 and line.count("bibitem") == 0 and newItem:
            if not newBlock:
                style += line
                '''
                if line.count(", and ") == 1:
                    style += line.replace(", and ", " y ")[:-1] #+ " \\\\\n"
                elif line.count(" and ") == 1:
                    style += line.replace(" and ", " y ")[:-1] #+ " \\\\\n"
                else:
                    style += line#[:-1] + " \\\\\n"
                '''
            else:
                style = style[:-4] + line[1:-1] + " \\\\\n"
        elif line.count("newblock") == 1:   
            style += line.replace("\n", " \\\\\n")
        elif newBlock:
            style = style[:-4] + line[1:-1] + " \\\\\n"
        elif newItem:
            style += line + "{\\bf "
        else:
            style += line

    fi.close()

    fo = file("thesis.bbl", "w")     
    fo.write(style)
    fo.close()




def usage():
  print 'Usage: ' + sys.argv[0] + ' <bibtex file>'
  print '    bibtex file:  File obtained after bibliography file parsed with bibtex tool'





if len(sys.argv) != 2:
  usage()
  sys.exit()


try:
    genBibStyle(sys.argv[1])
    print 'It has generated a new bibTex style called ' + sys.argv[1]
except:
  usage()


