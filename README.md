Templates
===
 * phd-uclm: Template for PhD documents
 * slides-beamerPB: Template for slides
 * journals: Contains some templates for several journals (ACM, Elsevier, IEEE)




Tools
===
 * bibliography: New bibliography style for PhD documents
 * systematic-review: Tool to perform systematic reviews
 * tables: Automatic generation of tables from systematic review. Tables contains a summary of the (categorized) problems of each work



Deprecated Templates
===
 * pfc-uclm: Old template for Final Project Degree (Now you can use https://bitbucket.org/arco_group/esi-tfg)
 * phd-proposal: Old template for PhD proposal (Now, you must use RAPI tool http://eid.uclm.es/procedimientos/procedimiento-documentoyplan/)